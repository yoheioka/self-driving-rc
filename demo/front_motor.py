import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from helpers import motor_driver
from time import sleep


def main():
    motor_driver.set_gpio_mode()
    motor_driver.set_front_motor_gpio()

    print "Going left"
    motor_driver.set_left_mode()
    sleep(2)

    print "Going backwards"
    motor_driver.set_right_mode()
    sleep(2)

    motor_driver.cleanup()


if __name__ == '__main__':
    main()
