import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from helpers import motor_driver
from time import sleep


def main():
    motor_driver.set_gpio_mode()
    motor_driver.set_back_motor_gpio()
    pwm = motor_driver.get_pwm_imstance()
    motor_driver.start_pwm(pwm)

    print "Going forwards"
    motor_driver.set_forward_mode()
    motor_driver.start_back_motor()
    sleep(2)

    motor_driver.stop_back_motor()

    print "Going backwards"
    motor_driver.set_reverse_mode()
    motor_driver.start_back_motor()
    sleep(2)

    motor_driver.stop_back_motor()
    motor_driver.cleanup()


if __name__ == '__main__':
    main()
