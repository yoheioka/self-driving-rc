import os
from shutil import copyfile


def main():
    for f in os.listdir('.'):
        ext = os.path.splitext(f)[1]
        if '.jpg' not in ext.lower():
            continue
        if 'idle' in f:
            copyfile(f, './idle/' + f)
        if 'left' in f:
            copyfile(f, './left/' + f)
        if 'right' in f:
            copyfile(f, './right/' + f)
        if 'reverse' in f:
            copyfile(f, './reverse/' + f)

if __name__ == '__main__':
    main()
